CREATE TABLE clientes(
    identificacion INT(10) NOT NULL,
    nombres VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    telefono VARCHAR(10),
    fechaRegistro DATE NOT NULL,
    PRIMARY KEY(identificacion),
    UNIQUE(email)
);

CREATE TABLE Juegos(
    idJuego INT NOT NULL,
    cantidad INT NOT NULL,
    categoria VARCHAR(20) NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    fechaLanzamiento VARCHAR(4) NOT NULL,
    protagonistas VARCHAR(50) NOT NULL,
    director VARCHAR(20) NOT NULL,
    productor VARCHAR(20) NOT NULL,
    tecnologia INT(3) NOT NULL,
    valorAlquiler INT(10) NOT NULL,
    PRIMARY KEY(idJuego)
);

CREATE TABLE technology(
    idConsola INT(3) NOT NULL,
    consola VARCHAR(20) NOT NULL,
    PRIMARY KEY(idConsola)
);

CREATE TABLE alquiler(
    idAlquiler INT NOT NULL,
    idJuegoAlq INT NOT NULL,
    idClienteAlq INT NOT NULL,
    fechaAlq DATE NOT NULL,
    tiempoAlq INT NOT NULL,
    precioAlq INT(10) NOT NULL,
    fechaDevolucion DATE,
    PRIMARY KEY(idAlquiler)
);

CREATE TABLE ventas(
    idVenta INT NOT NULL,
    idClienteVent INT NOT NULL,
    idAlquilerVent INT NOT NULL,
    idJuegoVent INT NOT NULL,
    totVenta INT,
    fechaVenta DATE,
    PRIMARY KEY(idVenta)
);

******************************************************************************
*****alquiler******
ALTER TABLE alquiler ADD FOREIGN KEY (idJuegoAlq) REFERENCES juegos (idJuego);
ALTER TABLE alquiler ADD FOREIGN KEY (idClienteAlq) REFERENCES clientes(identificacion);

*****ventas******
ALTER TABLE ventas ADD FOREIGN KEY (idAlquilerVent) REFERENCES alquiler(idAlquiler);
ALTER TABLE ventas ADD FOREIGN KEY (idClienteVent) REFERENCES clientes(identificacion);
ALTER TABLE ventas ADD FOREIGN KEY (idJuegoVent) REFERENCES juegos(idJuego);

*****juegos******
ALTER TABLE juegos ADD FOREIGN KEY (tecnologia) REFERENCES technology(idConsola);
