package comGamesModeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import comGamesModeloVO.JuegosVO;
import comGamesModeloVO.TechnologyVO;
import conGamesControlador.GameControlador;
import connection.Conexion;

public class TechnologyDAO {
	
	private static Conexion con;
	private static Connection connection; 
	
    public TechnologyDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {

        //System.out.println(jdbcURL);
        con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
    }
    
    //metodo para insertar los datos de una consola de juegos
    public static int agregarConsola(TechnologyVO addConsola) throws SQLException {
    	
    	int respuesta;
    	
    	String sql = "INSERT INTO technology (numConsola, nameConsola) VALUES(?,?)";
    	
    	con.conectar();
    	
    	connection = con.getJdbcConnection();
    	
        PreparedStatement st = connection.prepareStatement(sql);
        
        st.setInt(1, addConsola.getIdConsola());
        st.setString(2, addConsola.getConsola());
        
        boolean rta = st.executeUpdate() > 0;  
        
        if(rta) {
            respuesta = 1;
        }else {
            respuesta = 2;
        }

        st.close();
        con.desconectar();
        
        return respuesta;
    }
  //obtener el listado de tecnologias de consolas de la DB
    public List<TechnologyVO> listarTecnologias() throws SQLException {    
    	
        Statement st; //toma la consulta y la conexion a bd
        ResultSet rs; // donde se guardan los resultados de la consulta
        List<TechnologyVO> consolas = new ArrayList<TechnologyVO>();
        
        String sql;
        
        sql = "SELECT * FROM technology";
        
        try {

            con.conectar();

            connection = con.getJdbcConnection();

            st = connection.createStatement(); //se declara como se ejecutar la consulta , cuando no enviamos parametros
            rs = st.executeQuery(sql); //llamamos variable sql o query

            while (rs.next()){

                 int idTec = rs.getInt(1);
                 String nomTec = rs.getString(2);
                 
                 TechnologyVO listadoTecnologias = new TechnologyVO(idTec,nomTec);
                 consolas.add(listadoTecnologias);
            }
            connection.close();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return consolas;

    }

}
