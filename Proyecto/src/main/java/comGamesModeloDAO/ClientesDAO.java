package comGamesModeloDAO;

import java.util.List;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import comGamesModeloVO.ClientesVO;
import connection.Conexion;

public class ClientesDAO {

	private static Conexion con;
	private static Connection connection;

	public ClientesDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {

		// System.out.println(jdbcURL);
		con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
	}

	// metodo para insertar los datos de un cliente
	public static int agregarCliente(ClientesVO addCliente) throws SQLException {

		int respuesta;
		
		try {
            if(validarCliente(addCliente.getIdentificacion())== 1) {

            	String sql = "INSERT INTO clientes (identificacion, nombres, apellidos, email, telefono, fechaRegistro) "
            			+ "VALUES(?,?,?,?,?,CURRENT_DATE)";

        		con.conectar();

        		connection = con.getJdbcConnection();

        		PreparedStatement st = connection.prepareStatement(sql);

        		st.setInt(1, addCliente.getIdentificacion());
        		st.setString(2, addCliente.getNombres());
        		st.setString(3, addCliente.getApellidos());
        		st.setString(4, addCliente.getEmail());
        		st.setString(5, addCliente.getTelefono());

        		boolean rta = st.executeUpdate() > 0;

                if(rta) {
                    respuesta = 1;//registrado con existo
                }else {
                    respuesta = 4;//error al registrar
                }

                st.close();
                con.desconectar();
            }else {
                respuesta=3;//cliente duplicado
            }



        }catch (SQLException ex){
            if (ex.getMessage().contains("Duplicate entry")){
                respuesta = 2;//documento duplicado

            }else{
                respuesta = 4;//error al registrar cliente
            }

            System.out.println(ex.getMessage());
        }
        return respuesta;
	}

	// metodo para validar el usuario
	private static int validarCliente(int cc) throws SQLException {

		PreparedStatement stm = null;
		ResultSet rs = null;

		int rta = 1;

		String sql = "select * from clientes where identificacion =?";
		try {
			con.conectar();
			connection = con.getJdbcConnection();
			stm = connection.prepareStatement(sql);
			stm.setInt(1, cc);
			rs = stm.executeQuery();

			if (rs.next()) {

				rta = 3;

			} else {

				rta = 1;

			}

			rs.close();
			con.desconectar();
		} catch (SQLException ex) {
			System.out.print(ex.getMessage());
		}

		return rta;

	}

	// obtener el listado de clientes de la BD
	public List<ClientesVO> listarClientes() throws SQLException {

		Statement st; // toma la consulta y la conexion a bd
		ResultSet rs; // donde se guardan los resultados de la consulta
		List<ClientesVO> clientes = new ArrayList<ClientesVO>();

		String sql;

		sql = "SELECT * FROM clientes";

		try {

			con.conectar();

			connection = con.getJdbcConnection();

			st = connection.createStatement(); // se declara como se ejecutar la consulta , cuando no enviamos
												// parametros
			rs = st.executeQuery(sql); // llamamos variable sql o query

			while (rs.next()) {

				int identificacion = rs.getInt(1);
				String nombre = rs.getString(2);
				String apellido = rs.getString(3);
				String email = rs.getString(4);
				String telefono = rs.getString(5);
				Date registro = rs.getDate(6);

				ClientesVO listadoClientes = new ClientesVO(identificacion, nombre, apellido, email, telefono,
						registro);
				clientes.add(listadoClientes);
			}
			connection.close();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return clientes;

	}

}
