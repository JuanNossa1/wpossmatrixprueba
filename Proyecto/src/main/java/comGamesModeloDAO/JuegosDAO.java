package comGamesModeloDAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import comGamesModeloVO.AlquilerVO;
import comGamesModeloVO.JuegosVO;
import connection.Conexion;

public class JuegosDAO {
	
	private static Conexion con;
	private static Connection connection; 
	
    public JuegosDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {

        //System.out.println(jdbcURL);
        con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
    }
    
    //metodo para insertar los datos de un Juego
    public static int agregarJuego(JuegosVO addJuego) throws SQLException {
    	
    	int respuesta;
    	
    	String sql = "INSERT INTO juegos (idGame, cantDisponible, categoriaGame, nameGame, fLanzamiento, proGame, dirGame, productorGame, tipoConsola, vAlquiler) "
    			+ "VALUES(?,?,?,?,?,?,?,?,?,?)";
    	
    	con.conectar();
    	
    	connection = con.getJdbcConnection();
    	
        PreparedStatement st = connection.prepareStatement(sql);
        
        st.setInt(1, addJuego.getIdJuego());
        st.setInt(2, addJuego.getCantidad());
        st.setString(3, addJuego.getCategoria());
        st.setString(4, addJuego.getNombreJuego());
        st.setString(5, addJuego.getFechaLanzamiento());
        st.setString(6, addJuego.getProtagonistas());
        st.setString(7, addJuego.getDirector());
        st.setString(8, addJuego.getProductor());
        st.setInt(9, addJuego.getTecnologia());
        st.setInt(10, addJuego.getValorAlquiler());
        
        boolean rta = st.executeUpdate() > 0;  
        
        if(rta) {
            respuesta = 1;
        }else {
            respuesta = 2;
        }

        st.close();
        con.desconectar();
        
        return respuesta;
    }
    
  //obtener el listado de Juegos de la BD
    public List<JuegosVO> listarJuegos() throws SQLException {
    	
        Statement st; //toma la consulta y la conexion a bd
        ResultSet rs; // donde se guardan los resultados de la consulta
        List<JuegosVO> juegos = new ArrayList<JuegosVO>();
        
        String sql;
        
        sql = "SELECT * FROM juegos J INNER JOIN technology T ON idConsola WHERE T.idConsola = J.tecnologia";
        
        try {

            con.conectar();

            connection = con.getJdbcConnection();

            st = connection.createStatement(); //se declara como se ejecutar la consulta , cuando no enviamos parametros
            rs = st.executeQuery(sql); //llamamos variable sql o query

            while (rs.next()){

                 int idJuego = rs.getInt(1);
                 int cantidad = rs.getInt(2);
                 String categoria = rs.getString(3);
                 String nombreJuego = rs.getString(4);
                 String fechaLanzamiento = rs.getString(5);
                 String protagonistas = rs.getString(6);
                 String director = rs.getString(7);
                 String productor = rs.getString(8);
                 int tecnologia = rs.getInt(9);
                 int valorAlquiler = rs.getInt(10);
                 String nombreConsola = rs.getString(12);
                 
                 JuegosVO listadoJuegos = new JuegosVO(idJuego,cantidad,categoria,nombreJuego,fechaLanzamiento,protagonistas, director, productor, tecnologia, valorAlquiler, nombreConsola);
                 juegos.add(listadoJuegos);
                
            }
            connection.close();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return juegos;

    }
    
    //Actualizar Precio de los juegos
    public static int cambiarPrecio(JuegosVO modPrecioJuego) throws SQLException {
    	
    	int respuesta;
    	
    	String sql = "UPDATE juegos SET valorAlquiler = ? WHERE idJuego = ?";
        PreparedStatement st; //toma la consulta y la conexion a bd
        int rs = 0; // donde se guardan los resultados de la consulta
        
        try {

            con.conectar();
            connection = con.getJdbcConnection();

            st = connection.prepareStatement(sql);
            st.setInt(1, modPrecioJuego.getValorAlquiler());
            st.setInt(2, modPrecioJuego.getIdJuego());
            rs = st.executeUpdate(); //llamamos variable sql o query 
            
            boolean rta = st.executeUpdate() > 0;

			if (rta) {
				respuesta = 1;
			} else {
				respuesta = 4;
			}            
            connection.close();
            st.close();
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return rs;
    }

}
