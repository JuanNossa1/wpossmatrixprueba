package comGamesModeloDAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import comGamesModeloVO.AlquilerVO;
import comGamesModeloVO.VentasVO;
import connection.Conexion;

public class VentasDAO {
	
	private static Conexion con;
	private static Connection connection; 
	
    public VentasDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {

        //System.out.println(jdbcURL);
        con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
    }
    
    //metodo para insertar los datos de una Venta
    public static int agregarVenta(VentasVO addVenta) throws SQLException {
    	
    	int respuesta;
    	
    	String sql = "INSERT INTO ventas (clienteVenta, alquilerVenta, juegoVenta, totalVenta) "
    			+ "VALUES(?,?,?,?)";
    	
    	con.conectar();
    	
    	connection = con.getJdbcConnection();
    	
        PreparedStatement st = connection.prepareStatement(sql);
        
        st.setInt(1, addVenta.getIdClienteVent());
        st.setInt(2, addVenta.getIdAlquilerVent());
        st.setInt(3, addVenta.getIdJuegoVent());
        st.setInt(4, addVenta.getTotVenta());
        
        boolean rta = st.executeUpdate() > 0;  
        
        if(rta) {
            respuesta = 1;
        }else {
            respuesta = 2;
        }

        st.close();
        con.desconectar();
        
        return respuesta;
    }
    
    //obtener las ventas del d�a
    public List<VentasVO> listarVentas() throws SQLException {
    	
        Statement st; //toma la consulta y la conexion a bd
        ResultSet rs; // donde se guardan los resultados de la consulta
        List<VentasVO> ventasDia = new ArrayList<VentasVO>();
        
        String sql;
        
        sql = "SELECT V.fechaVenta, V.idVenta, V.idClienteVent, V.idAlquilerVent, V.idJuegoVent, J.nombreJuego, T.consola, V.totVenta "
        		+ "FROM ventas V "
        		+ "INNER JOIN juegos J ON V.idJuegoVent = J.idJuego "
        		+ "INNER JOIN technology T ON J.tecnologia = T.idConsola "
        		+ "WHERE V.fechaVenta = CURRENT_DATE";
        
        try {
        	
            con.conectar();
            connection = con.getJdbcConnection();
            st = connection.createStatement();
            rs = st.executeQuery(sql);

            while (rs.next()){

                 Date fechaVenta = rs.getDate(1);
                 int idVenta = rs.getInt(2);
                 int idClienteVent = rs.getInt(3);
                 int idAlquilerVent = rs.getInt(4);
                 int idJuegoVent = rs.getInt(5);
                 String nomJuego = rs.getString(6);
                 String nomConsola = rs.getString(7);
                 int precioAlq = rs.getInt(8);
                 
                 VentasVO listadoVentas = new VentasVO(idVenta, idClienteVent, idAlquilerVent, idJuegoVent, precioAlq, fechaVenta, nomJuego, nomConsola);
                 ventasDia.add(listadoVentas);
            }
            connection.close();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return ventasDia;
    }

}
