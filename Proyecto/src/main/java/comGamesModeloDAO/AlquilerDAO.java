package comGamesModeloDAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import comGamesModeloVO.AlquilerVO;
import comGamesModeloVO.VentasVO;
import connection.Conexion;

public class AlquilerDAO {

	private static Conexion con;
	private static Connection connection;

	public AlquilerDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {

		// System.out.println(jdbcURL);
		con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
	}

	// metodo para insertar los datos de un Alquiler y en una venta
	public static int agregarAlquiler(AlquilerVO addAlquiler) throws SQLException {

		int respuesta;

		try {
			if (validarCliente(addAlquiler.getIdClienteAlq()) == 1) {
				if (validarJuego(addAlquiler.getIdJuegoAlq()) == 1) {

					String sql = "INSERT INTO alquiler (idJuegoAlq, idClienteAlq, fechaAlq, tiempoAlq, precioAlq, fechaDevolucion) "
							+ "VALUES(?,?,CURRENT_DATE,?,"
							+ valorAlquiler(addAlquiler.getIdJuegoAlq(), addAlquiler.getTiempoAlq())
							+ ",DATE_ADD(CURRENT_DATE, INTERVAL ?*7 DAY))";
					String sql2 = " INSERT INTO ventas (idClienteVent, idAlquilerVent, idJuegoVent, totVenta) VALUES ("
							+ addAlquiler.getIdClienteAlq() + "," + idAlquiler() + "," + addAlquiler.getIdJuegoAlq()
							+ "," + valorAlquiler(addAlquiler.getIdJuegoAlq(), addAlquiler.getTiempoAlq()) + ")";

					con.conectar();

					connection = con.getJdbcConnection();

					PreparedStatement st = connection.prepareStatement(sql);
					PreparedStatement st2 = connection.prepareStatement(sql2);

//					st.setInt(1, addAlquiler.getIdAlquiler());
					st.setInt(1, addAlquiler.getIdJuegoAlq());
					st.setInt(2, addAlquiler.getIdClienteAlq());
					// st.setDate(4, addAlquiler.getFechaAlq());
					st.setInt(3, addAlquiler.getTiempoAlq());
//					st.setInt(6, addAlquiler.getPrecioAlq());
					st.setInt(4, addAlquiler.getTiempoAlq());

					boolean rta = st.executeUpdate() > 0;
					st2.executeUpdate();

					if (rta) {
						respuesta = 1;
					} else {
						respuesta = 4;
					}

					st.close();
					con.desconectar();
				} else {
					respuesta = 3;// juego no existe
				}
			} else {
				respuesta = 2;// cliente no existe
			}

		} catch (SQLException ex) {
			if (ex.getMessage().contains("Duplicate entry")) {
				respuesta = 2;// campo no existe
			} else {
				respuesta = 4;// error al registrar alquiler
			}

			System.out.println(ex.getMessage());
		}

		return respuesta;
	}

	// metodo para traer el valor de alquiler y agregar el nuevo
	public static int valorAlquiler(int precio, int tiempo) {

		PreparedStatement stm = null;
		ResultSet rs = null;

		int rta = 0;
		String sql = "SELECT valorAlquiler FROM juegos WHERE idJuego = ?";

		try {
			con.conectar();
			connection = con.getJdbcConnection();
			stm = connection.prepareStatement(sql);
			stm.setInt(1, precio);
			rs = stm.executeQuery();

			if (rs.next()) {

				rta = rs.getInt(1);
				rta = rta * tiempo;

			}
			rs.close();
			con.desconectar();
		} catch (SQLException ex) {
			System.out.print(ex.getMessage());
		}

		return rta;
	}

	// metodo para obtener el id del Alquiler
	public static int idAlquiler() {

		Statement stm = null;
		ResultSet rs = null;

		int rta = 0;
		String sql = "SELECT MAX(idAlquiler) FROM alquiler";

		try {
			con.conectar();
			connection = con.getJdbcConnection();
			stm = connection.createStatement();
			rs = stm.executeQuery(sql);

			if (rs.next()) {

				rta = rs.getInt(1);
				rta = rta + 1;

			}
			rs.close();
			con.desconectar();
		} catch (SQLException ex) {
			System.out.print(ex.getMessage());
		}

		return rta;
	}

	// obtener el listado de Alquileres de la BD
	public List<AlquilerVO> listarAlquileres() throws SQLException {

		Statement st; // toma la consulta y la conexion a bd
		ResultSet rs; // donde se guardan los resultados de la consulta
		List<AlquilerVO> alquileres = new ArrayList<AlquilerVO>();

		String sql;

		sql = "SELECT * FROM alquiler";

		try {

			con.conectar();

			connection = con.getJdbcConnection();

			st = connection.createStatement(); // se declara como se ejecutar la consulta , cuando no enviamos
												// parametros
			rs = st.executeQuery(sql); // llamamos variable sql o query

			while (rs.next()) {

				int idAlquiler = rs.getInt(1);
				int idJuegoAlq = rs.getInt(2);
				int idClienteAlq = rs.getInt(3);
				Date fechaAlq = rs.getDate(4);
				int tiempoAlq = rs.getInt(5);
				int precioAlq = rs.getInt(6);
				Date fechaDevolucion = rs.getDate(7);

				AlquilerVO listadoAlquileres = new AlquilerVO(idAlquiler, idJuegoAlq, idClienteAlq, fechaAlq, tiempoAlq,
						precioAlq, fechaDevolucion,null,null);
				alquileres.add(listadoAlquileres);
			}
			connection.close();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return alquileres;
	}

	

	// metodo para validar el usuario
	private static int validarCliente(int cc) throws SQLException {

		PreparedStatement stm = null;
		ResultSet rs = null;

		int rta = 1;

		String sql = "select * from clientes where identificacion =?";
		try {
			con.conectar();
			connection = con.getJdbcConnection();
			stm = connection.prepareStatement(sql);
			stm.setInt(1, cc);
			rs = stm.executeQuery();

			if (rs.next()) {

				rta = 1;

			} else {

				rta = 2;

			}

			rs.close();
			con.desconectar();
		} catch (SQLException ex) {
			System.out.print(ex.getMessage());
		}

		return rta;

	}

	// metodo para validar el Id del juego
	private static int validarJuego(int game) throws SQLException {

		PreparedStatement stm = null;
		ResultSet rs = null;

		int rta = 1;

		String sql = "select * from juegos where idJuego =?";
		try {
			con.conectar();
			connection = con.getJdbcConnection();
			stm = connection.prepareStatement(sql);
			stm.setInt(1, game);
			rs = stm.executeQuery();

			if (rs.next()) {

				rta = 1;

			} else {

				rta = 3;

			}

			rs.close();
			con.desconectar();
		} catch (SQLException ex) {
			System.out.print(ex.getMessage());
		}

		return rta;

	}
	// obtener listado alquileres por cliente
	
	public List<AlquilerVO> listarAlquieresXCliente() throws SQLException {
    	
        Statement st; //toma la consulta y la conexion a bd
        ResultSet rs; // donde se guardan los resultados de la consulta
        List<AlquilerVO> listXcliente = new ArrayList<AlquilerVO>();
        
        String sql;
        
        sql = "SELECT A.idAlquiler, A.idClienteAlq, J.nombreJuego, T.consola, A.tiempoAlq, A.fechaAlq, A.fechaDevolucion "
        		+ "FROM alquiler A "
        		+ "INNER JOIN juegos J ON A.idJuegoAlq = J.idJuego "
        		+ "INNER JOIN technology T ON J.tecnologia = T.idConsola";
        
        try {
        	
            con.conectar();
            connection = con.getJdbcConnection();
            st = connection.createStatement();
            rs = st.executeQuery(sql);

            while (rs.next()){

            	 int idAlq = rs.getInt(1);
                 int idCliente = rs.getInt(2);
                 String nomJuego = rs.getString(3);
                 String nomConsola = rs.getString(4);
                 int tiempoAlq = rs.getInt(5);
                 Date fechaAlq = rs.getDate(6);
                 Date fechaDev = rs.getDate(7);

                 
                 AlquilerVO lAlqXCli = new AlquilerVO(idAlq, 0, idCliente, fechaAlq, tiempoAlq, 0, fechaDev, nomJuego, nomConsola);
                 listXcliente.add(lAlqXCli);
            }
            connection.close();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return listXcliente;
    }

}
