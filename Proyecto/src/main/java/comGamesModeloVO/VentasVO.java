package comGamesModeloVO;

import java.sql.Date;

public class VentasVO {
	private int idVenta;
	private int idClienteVent;
	private int idAlquilerVent;
	private int idJuegoVent;
	private int totVenta;
	private Date fechaVenta;
	private String nombreJuego;
	private String nameConsola;
	
	
	public VentasVO(int idVenta, int idClienteVent, int idAlquilerVent, int idJuegoVent, int totVenta, Date fechaVenta, String nombreJuego, String nameConsola){
		this.idVenta = idVenta;
		this.idClienteVent = idClienteVent;
		this.idAlquilerVent = idAlquilerVent;
		this.idJuegoVent = idJuegoVent;
		this.totVenta = totVenta;
		this.fechaVenta = fechaVenta;
		this.nombreJuego = nombreJuego;
		this.nameConsola = nameConsola;
	}

	public int getTotVenta() {
		return totVenta;
	}

	public void setTotVenta(int totVenta) {
		this.totVenta = totVenta;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public int getIdClienteVent() {
		return idClienteVent;
	}

	public void setIdClienteVent(int idClienteVent) {
		this.idClienteVent = idClienteVent;
	}

	public int getIdAlquilerVent() {
		return idAlquilerVent;
	}

	public void setIdAlquilerVent(int idAlquilerVent) {
		this.idAlquilerVent = idAlquilerVent;
	}

	public int getIdJuegoVent() {
		return idJuegoVent;
	}

	public void setIdJuegoVent(int idJuegoVent) {
		this.idJuegoVent = idJuegoVent;
	}

	public String getNombreJuego() {
		return nombreJuego;
	}

	public void setNombreJuego(String nombreJuego) {
		this.nombreJuego = nombreJuego;
	}

	public String getNameConsola() {
		return nameConsola;
	}

	public void setNameConsola(String nameConsola) {
		this.nameConsola = nameConsola;
	}
}
