package comGamesModeloVO;

public class TechnologyVO {
	private int idConsola;
	private String consola;
	
	public TechnologyVO(int idConsola, String consola){
		this.idConsola = idConsola;
		this.consola = consola;
	}

	public int getIdConsola() {
		return idConsola;
	}

	public void setIdConsola(int idConsola) {
		this.idConsola = idConsola;
	}

	public String getConsola() {
		return consola;
	}

	public void setConsola(String consola) {
		this.consola = consola;
	}
}
