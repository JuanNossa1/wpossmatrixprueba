package comGamesModeloVO;

import java.sql.Date;

public class AlquilerVO {
	
	private int idAlquiler;
	private int idJuegoAlq;
	private int idClienteAlq;
	private Date fechaAlq;
	private int tiempoAlq;
	private int precioAlq;
	private Date fechaDevolucion;
	private String nombreJuego;
	private String nombreConsola;
	
	public AlquilerVO(	int idAlquiler, int idJuegoAlq, int idClienteAlq, Date fechaAlq, int tiempoAlq, int precioAlq, Date fechaDevolucion, String nombreJuego, String nombreConsola){
		this.idAlquiler = idAlquiler;
		this.idJuegoAlq = idJuegoAlq;
		this.idClienteAlq = idClienteAlq;
		this.fechaAlq = fechaAlq;
		this.tiempoAlq = tiempoAlq;
		this.precioAlq = precioAlq;
		this.fechaDevolucion = fechaDevolucion;
		this.nombreJuego = nombreJuego;
		this.nombreConsola = nombreConsola;
	}

	public String getNombreJuego() {
		return nombreJuego;
	}

	public void setNombreJuego(String nombreJuego) {
		this.nombreJuego = nombreJuego;
	}

	public String getNombreConsola() {
		return nombreConsola;
	}

	public void setNombreConsola(String nombreConsola) {
		this.nombreConsola = nombreConsola;
	}

	public int getIdAlquiler() {
		return idAlquiler;
	}

	public void setIdAlquiler(int idAlquiler) {
		this.idAlquiler = idAlquiler;
	}

	public int getIdJuegoAlq() {
		return idJuegoAlq;
	}

	public void setIdJuegoAlq(int idJuegoAlq) {
		this.idJuegoAlq = idJuegoAlq;
	}

	public int getIdClienteAlq() {
		return idClienteAlq;
	}

	public void setIdClienteAlq(int idClienteAlq) {
		this.idClienteAlq = idClienteAlq;
	}

	public Date getFechaAlq() {
		return fechaAlq;
	}

	public void setFechaAlq(Date fechaAlq) {
		this.fechaAlq = fechaAlq;
	}

	public int getTiempoAlq() {
		return tiempoAlq;
	}

	public void setTiempoAlq(int tiempoAlq) {
		this.tiempoAlq = tiempoAlq;
	}

	public int getPrecioAlq() {
		return precioAlq;
	}

	public void setPrecioAlq(int precioAlq) {
		this.precioAlq = precioAlq;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

}
