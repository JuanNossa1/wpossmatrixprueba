package comGamesModeloVO;

public class JuegosVO {
	
	private int idJuego;
	private int cantidad;
	private String categoria;
	private String nombreJuego;
	private String fechaLanzamiento;
	private String protagonistas;
	private String director;
	private String productor;
	private int tecnologia;
	private int valorAlquiler;
	private String nombreConsola;
	

	public JuegosVO(int idJuego, int cantidad, String categoria, String nombreJuego, String fechaLanzamiento, 
			String protagonistas, String director, String productor, int tecnologia, int valorAlquiler, String nombreConsola) {
		this.idJuego = idJuego;
		this.cantidad = cantidad;
		this.categoria = categoria;
		this.nombreJuego = nombreJuego;
		this.fechaLanzamiento = fechaLanzamiento;
		this.protagonistas = protagonistas;
		this.director = director;
		this.productor = productor;
		this.tecnologia = tecnologia;
		this.valorAlquiler = valorAlquiler;
		this.nombreConsola = nombreConsola;
	}
	
	public String getNombreConsola() {
		return nombreConsola;
	}

	public void setNombreConsola(String nombreConsola) {
		this.nombreConsola = nombreConsola;
	}
	
	public int getIdJuego() {
		return idJuego;
	}

	public void setIdJuego(int idJuego) {
		this.idJuego = idJuego;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getNombreJuego() {
		return nombreJuego;
	}

	public void setNombreJuego(String nombreJuego) {
		this.nombreJuego = nombreJuego;
	}

	public String getFechaLanzamiento() {
		return fechaLanzamiento;
	}

	public void setFechaLanzamiento(String fechaLanzamiento) {
		this.fechaLanzamiento = fechaLanzamiento;
	}

	public String getProtagonistas() {
		return protagonistas;
	}

	public void setProtagonistas(String protagonistas) {
		this.protagonistas = protagonistas;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getProductor() {
		return productor;
	}

	public void setProductor(String productor) {
		this.productor = productor;
	}

	public int getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(int tecnologia) {
		this.tecnologia = tecnologia;
	}

	public int getValorAlquiler() {
		return valorAlquiler;
	}

	public void setValorAlquiler(int valorAlquiler) {
		this.valorAlquiler = valorAlquiler;
	}
}
