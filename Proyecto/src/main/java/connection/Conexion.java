package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	
	//atributos clase
	private Connection jdbcConnection;
	private String jdbcURL;
    private String jdbcUsername;
    private String jdbcPassword;
	
	//metodo constructor
	public Conexion(String jdbcURL, String jdbcUsername, String jdbcPassword) {
		this.jdbcURL = jdbcURL;
		this.jdbcUsername = jdbcUsername;
		this.jdbcPassword = jdbcPassword;	
	}
	
	//metodo para abrir conexion
	public void conectar() throws SQLException{
		if (jdbcConnection == null || jdbcConnection.isClosed()) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				System.out.println("Driver Cargado");
			} catch (ClassNotFoundException e) {
				// TODO: handle exception
				System.out.println("Fallo la Carga del Driver");
				System.out.println(e.getMessage());
//				throw new SQLException(e);
			}
			
			jdbcConnection = DriverManager.getConnection(jdbcURL,jdbcUsername, jdbcPassword);
			
			if(jdbcConnection!=null) {
	            System.out.println("Conexi�n al Server Exitosa");
	          }
			else {
				System.out.println("Conexi�n al Server Fallida");
			}
		}
	}
	
    public Connection getJdbcConnection() {
        return jdbcConnection;
    }
    
    //cerrar conexion con BD
    public void desconectar() throws SQLException {

        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }
	
//	public static void main(String[] args) throws SQLException {
//		
//		String jdbcUsername = "root";
//		String jdbcPassword = "";
//		String jdbcURL = "jdbc:mysql://localhost:3306/wpossgames";
//		
//		Conexion con= new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
//		
//		
//		try {
//
//            con.conectar();
//
//           
//        }catch(SQLException ex){
//            System.out.println(ex.getMessage());
//        }
//	}
}
