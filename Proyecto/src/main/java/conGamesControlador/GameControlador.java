package conGamesControlador;

import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comGamesModeloDAO.AlquilerDAO;
import comGamesModeloDAO.ClientesDAO;
import comGamesModeloDAO.JuegosDAO;
import comGamesModeloDAO.TechnologyDAO;
import comGamesModeloDAO.VentasDAO;
import comGamesModeloVO.AlquilerVO;
import comGamesModeloVO.ClientesVO;
import comGamesModeloVO.JuegosVO;
import comGamesModeloVO.TechnologyVO;
import comGamesModeloVO.VentasVO;

/**
 * Servlet implementation class GameControlador
 */
@WebServlet("/GameControlador")
public class GameControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;

    //instanciamos un objeto de la clase UsuarioDao de manera global
    AlquilerDAO alquilerDAO;
    ClientesDAO clientesDAO;
    JuegosDAO juegosDAO;
    TechnologyDAO techDAO;
    VentasDAO ventasDAO;


    //Metodo para ejecutar cuando se llama el controlador
    public void init() {
        String jdbcURL = getServletContext().getInitParameter("jdbcURL");
        String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
        String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");

        try {
        	alquilerDAO = new AlquilerDAO(jdbcURL, jdbcUsername, jdbcPassword);
        	clientesDAO= new ClientesDAO(jdbcURL, jdbcUsername, jdbcPassword);
        	juegosDAO= new JuegosDAO(jdbcURL, jdbcUsername, jdbcPassword);
        	techDAO= new TechnologyDAO(jdbcURL, jdbcUsername, jdbcPassword);
        	ventasDAO = new VentasDAO(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    
    public GameControlador() {
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher;
        
        String action = request.getParameter("action");
        if(action != null){
            try {
                switch (action) {
                    case "addCliente":
                        agregarC(request, response);
                        break;
                        
                    case "addAlquilar":
                        alquilarG(request, response);
                        break;
                        
                    case "modPrecio":
                        modificarP(request, response);
                        break;
                        
                    case "consultarVentas":
                        consultarV(request, response);
                        break;
                        
                    case "consultarAlquileres":
                        consultarA(request, response);
                        break;
                        
                    case "indexPpal":
                        redirectIndex(request, response);
                        break;
                        
                    case "registrarUsuario":
                        regUsuario(request, response);
                        break;
                        		
                    case "alquilarJuego":
                        alqGame(request, response);
                        break;  
                        
                    case "modificarCostoAlquiler":
                        modAlqPrecio(request, response);
                        break;

                    default:
                        break;
                }
            }catch(Exception ex){
                        System.out.print(ex.getMessage());
            }
        }
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	//funcion para redirigir al formulario de agregar cliente
	private void agregarC(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        dispatcher = request.getRequestDispatcher("agregarCliente.jsp");
        dispatcher.forward(request, response);
    }
	//funcion para redirigir al formulario de alquilar juego
	private void alquilarG(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        List<JuegosVO> listaJuegos = juegosDAO.listarJuegos();     
        request.setAttribute("listaJuegos", listaJuegos);
        dispatcher = request.getRequestDispatcher("alquilarJuego.jsp");
        dispatcher.forward(request, response);
    }
	//funcion para redirigir al formulario de modificar valor alquileres
	private void modificarP(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        List<JuegosVO> listaJuegos = juegosDAO.listarJuegos();     
        request.setAttribute("listaJuegos", listaJuegos);
        dispatcher = request.getRequestDispatcher("modPrecioAlquiler.jsp");
        dispatcher.forward(request, response);
    }
	//funcion para redirigir a la vista de consultar ventas del d�a
	private void consultarV(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        List<VentasVO> listaVentas = ventasDAO.listarVentas();     
        request.setAttribute("listaVentas", listaVentas);
        dispatcher = request.getRequestDispatcher("consultarVentas.jsp");
        dispatcher.forward(request, response);
    }
	//funcion para redirigir formulario de consulta alquileres por cliente
	private void consultarA(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        List<AlquilerVO> listaAlquileres = alquilerDAO.listarAlquieresXCliente();     
        request.setAttribute("listaAlquileres", listaAlquileres);
        dispatcher = request.getRequestDispatcher("consultarAlquileres.jsp");
        dispatcher.forward(request, response);
    }
	//funcion para redirigir al menu principal
	private void redirectIndex(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
	//funcion para realizar el envio de datos de un cliente a registrar
	private void regUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        
        RequestDispatcher dispatcher;
        String name, last_name, correo, phone;
        int numberId, flagInsert;

        numberId = Integer.parseInt(request.getParameter("txtIdentidad"));
        name = request.getParameter("txtNombre");
        last_name = request.getParameter("txtApellidos");
        correo = request.getParameter("txtEmail");
        phone = request.getParameter("txtTelefono");
        
        ClientesVO usuario = new ClientesVO(numberId, name, last_name, correo, phone, null);
        flagInsert = ClientesDAO.agregarCliente(usuario);
        if (flagInsert==1) {

            request.setAttribute("flag", flagInsert);
            dispatcher = request.getRequestDispatcher("agregarCliente.jsp");
        }
        else {
            request.setAttribute("usuario",usuario );
            request.setAttribute("flag", flagInsert);
            dispatcher = request.getRequestDispatcher("agregarCliente.jsp");
        }

        dispatcher.forward(request, response);
    }
	//funcion para realizar el envio de datos de un juego a alquiler e insertar la venta
	private void alqGame(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        
        RequestDispatcher dispatcher;

        int idJuego, idCliente, tiempoAlq, flagInsert;

        idJuego = Integer.parseInt(request.getParameter("txtIdJuego"));
        idCliente = Integer.parseInt(request.getParameter("txtIdCliente"));
        tiempoAlq = Integer.parseInt(request.getParameter("txtTiempoAlq"));
        
        AlquilerVO alquiler = new AlquilerVO(0, idJuego, idCliente, null, tiempoAlq, 0, null,null,null);
        flagInsert = AlquilerDAO.agregarAlquiler(alquiler);
        if (flagInsert==1) {

            request.setAttribute("flag", flagInsert);
            dispatcher = request.getRequestDispatcher("alquilarJuego.jsp");
        }
        else {
            request.setAttribute("alquiler",alquiler );
            request.setAttribute("flag", flagInsert);
            dispatcher = request.getRequestDispatcher("alquilarJuego.jsp");
        }

        dispatcher.forward(request, response);
    }	
	//funcion para realizar la actualizaci�n de los precios de los juegos 
	private void modAlqPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException,IOException {
        
        PrintWriter out = response.getWriter();
        RequestDispatcher dispatcher;
        List<JuegosVO> listaJuegos = juegosDAO.listarJuegos();     
        request.setAttribute("listaJuegos", listaJuegos);
        dispatcher = request.getRequestDispatcher("modPrecioAlquiler.jsp");
        dispatcher.forward(request, response);
    }
	
}
