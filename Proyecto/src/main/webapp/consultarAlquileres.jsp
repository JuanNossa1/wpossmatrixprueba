<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<meta charset="ISO-8859-1">
<title>WPOSS GAMES</title>
</head>
<body>
	<!-- MENSAJES ALQUILAR JUEGO -->
	<%
	if (request.getAttribute("flag") != null) {
		int prueba = (Integer) request.getAttribute("flag");
		if (prueba == 1) {
			request.setAttribute("flag", 0);
			out.println(
			"<script type=\"text/javascript\">Swal.fire('Operaci�n exitosa!', 'Cliente Encontrado!.', 'success')</script>");
		} else if (prueba == 2) {

			request.setAttribute("flag", 0);
			out.println("<script type=\"text/javascript\">Swal.fire('Error!', 'El cliente no Existe', 'error')</script>");
		} else if (prueba == 3) {
			request.setAttribute("flag", 0);
			out.println("<script type=\"text/javascript\">Swal.fire('Error!', 'El juego no existe.', 'error')</script>");
		} else if (prueba == 4) {
			request.setAttribute("flag", 0);
			out.println(
			"<script type=\"text/javascript\">Swal.fire('Error!', 'Ha ocurrido un error, por favor intente nuevamente..', 'error')</script>");
		}
	}
	%>
	<div class="wrapper d-flex align-items-stretch">
		<nav id="sidebar">
			<div class="custom-menu">
				<button type="button" id="sidebarCollapse" class="btn btn-primary">
					<i class="fa fa-bars"></i> <span class="sr-only">Games Menu</span>
				</button>
			</div>
			<div class="p-4 pt-5">
				<h1>
					<a href="GameControlador?action=indexPpal" class="logo">Men�</a>
				</h1>
				<ul class="list-unstyled components mb-5">
					<li class="active"><a href="GameControlador?action=addCliente">Agregar
							Cliente</a></li>
					<li><a href="GameControlador?action=addAlquilar">Alquilar
							Juego</a></li>
					<li><a href="GameControlador?action=modPrecio">Modificar
							Precio Alquiler</a></li>
					<li><a href="GameControlador?action=consultarVentas">Consultar
							Ventas</a></li>
					<li><a href="GameControlador?action=consultarAlquileres">Consultar
							Alquileres por Cliente</a></li>
				</ul>

			</div>
		</nav>
		
		<div id="content">
		<!-- Page Content  -->
		<div class="container">
		<div class="form-header">
			<br>
			<br>
			<h3>CLIENTES QUE HAN ALQUILADO JUEGOS</h3>
		</div>
			<table
				class="table table-striped table-bordered text-center table-hover table-condensed"
				style="margin-top: 20px;">
				<thead>
					<tr>
						<td>ID ALQUILER</td>
						<td>ID CLIENTE</td>
						<td>NOMBRE JUEGO</td>
						<td>CONSOLA</td>
						<td>TIEMPO ALQUILER</td>
						<td>FECHA ALQUILER</td>
						<td>FECHA DEVOLUCION</td>
					</tr>
				</thead>
				<tbody>
					<!--Lista de las ventas del d�a  -->
					<c:forEach var="alquieresXClientes" items="${listaAlquileres}">
						<tr>
							<td>${alquieresXClientes.getIdAlquiler()}</td>
							<td>${alquieresXClientes.getIdClienteAlq()}</td>
							<td>${alquieresXClientes.getNombreJuego()}</td>
							<td>${alquieresXClientes.getNombreConsola()}</td>
							<td>${alquieresXClientes.getTiempoAlq()} SEMANAS</td>
							<td>${alquieresXClientes.getFechaAlq()}</td>
							<td>${alquieresXClientes.getFechaDevolucion()}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		</div>

	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>