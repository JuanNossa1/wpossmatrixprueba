<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css">
	<meta charset="ISO-8859-1">
<title>WPOSS GAMES</title>
</head>
<body>
<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	          <i class="fa fa-bars"></i>
	          <span class="sr-only">Games Menu</span>
	        </button>
        </div>
				<div class="p-4 pt-5">
		  		<h1><a href="GameControlador?action=indexPpal" class="logo">Men�</a></h1>
	        <ul class="list-unstyled components mb-5">
	          <li class="active">
	            <a href="GameControlador?action=addCliente">Agregar Cliente</a>
	          </li>
	          <li>
	            <a href="GameControlador?action=addAlquilar">Alquilar Juego</a>
	          </li>
	          <li>
              	<a href="GameControlador?action=modPrecio">Modificar Precio Alquiler</a>
	          </li>
	          <li>
              	<a href="GameControlador?action=consultarVentas">Consultar Ventas</a>
	          </li>
	          <li>
              	<a href="GameControlador?action=consultarAlquileres">Consultar Mis Alquileres</a>
	          </li>
	        </ul>	        

	      </div>
    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
      		<h1><center>BIENVENIDO A WPOSS GAMES</center></h1>
      		<br>
      		<Img src = "img/ppalLogo.jpg" width="100%" height="80%"/>
      </div>
		</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>