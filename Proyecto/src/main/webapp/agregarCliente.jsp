<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<meta charset="ISO-8859-1">

<title>WPOSS GAMES</title>
</head>
<body>

<!-- MENSAJES AGREGAR CLIENTES -->
<%
    if (request.getAttribute("flag")!=null){
        int prueba =  (Integer)request.getAttribute("flag");
        if ( prueba == 1){
            request.setAttribute("flag",0);
            out.println("<script type=\"text/javascript\">Swal.fire('Operaci�n exitosa!', 'Usuario registrado de forma correcta!.', 'success')</script>");
        }else if( prueba == 2){

            request.setAttribute("flag",0);
            out.println("<script type=\"text/javascript\">Swal.fire('Error!', 'Ya existe un usuario con el documento ingresado.', 'error')</script>");
        }else if( prueba == 3){
            request.setAttribute("flag",0);
            out.println("<script type=\"text/javascript\">Swal.fire('Error!', 'Ya existe un usuario con el documento ingresado.', 'error')</script>");
        }else if( prueba == 4){
            request.setAttribute("flag",0);
            out.println("<script type=\"text/javascript\">Swal.fire('Error!', 'Ha ocurrido un error, por favor intente nuevamente..', 'error')</script>");
        }
    }
%>
<!-- FORMULARIO PARA AGREGAR CLIENTES -->
	<div class="wrapper d-flex align-items-stretch">
		<nav id="sidebar">
			<div class="custom-menu">
				<button type="button" id="sidebarCollapse" class="btn btn-primary">
					<i class="fa fa-bars"></i> <span class="sr-only">Games Menu</span>
				</button>
			</div>
			<div class="p-4 pt-5">
				<h1>
					<a href="GameControlador?action=indexPpal" class="logo">Men�</a>
				</h1>
				<ul class="list-unstyled components mb-5">
					<li class="active"><a href="GameControlador?action=addCliente">Agregar
							Cliente</a></li>
					<li><a href="GameControlador?action=addAlquilar">Alquilar
							Juego</a></li>
					<li><a href="GameControlador?action=modPrecio">Modificar
							Precio Alquiler</a></li>
					<li><a href="GameControlador?action=consultarVentas">Consultar
							Ventas</a></li>
					<li><a href="GameControlador?action=consultarAlquileres">Consultar
							Alquileres por Cliente</a></li>
				</ul>

			</div>
		</nav>

		<!-- Formulario registrar nuevo usuario  -->
		<div id="content" class="p-4 p-md-5 pt-5">
			<div class="wrapper">
				<form class="formulario" id="formulario"
					action="GameControlador?action=registrarUsuario" method="post"
					onsubmit="return validarformulario(event)">
					<div class="form-header">
						<h3>Registro</h3>
					</div>
					<div id="wizard">
						<h4></h4>
						<!-- Campo identidad -->
						<div class="col-12 col-lg-6 " id="campo_identidad">
							<label for="idIdentidad" class="formulario_label">Documento
								Identidad</label>
							<div class="formulario_grupo_input mb-1">
								<input type="text"
									class="form-control formulario_input_formulario"
									name="txtIdentidad" id="cedula" autofocus required
									placeholder="Digite el n�mero de identificaci�n del cliente"
									value=""> <i class="formulario__validacion_estado"></i>
							</div>
							<!-- <p class="formulario_input_error mb-2">M�nimo 3 caracteres</p> -->
						</div>
						<br>
						<!-- Campo nombres -->
						<div class="col-12 col-lg-6" id="campo_nombres">
							<label for="nombres" class="formulario_label">Nombres</label>
							<div class="formulario_grupo_input mb-1">
								<input type="text"
									class="form-control formulario_input_formulario"
									name="txtNombre" id="nombre" required
									placeholder="Ingrese los nombres" value=""> <i
									class="formulario__validacion_estado"></i>
							</div>
							<!-- <p class="formulario_input_error mb-2">N?mero de documento no v?lido</p> -->
						</div>
						<br>
						<!-- Campo apellidos -->
						<div class="col-12 col-lg-6" id="campo_apellidos">
							<label for="apellidos" class="formulario_label">Apellidos</label>
							<div class="formulario_grupo_input mb-1">
								<input type="text"
									class="form-control formulario_input_formulario"
									name="txtApellidos" id="apellido" required
									placeholder="Ingrese los apellidos" value=""> <i
									class="formulario__validacion_estado"></i>
							</div>
							<!-- <p class="formulario_input_error mb-2">Introduce una direcci?n correcta</p> -->
						</div>
						<br>
						<!-- Campo email -->
						<div class="col-12 col-lg-6" id="campo_email">
							<label for="email" class="formulario_label">Email</label>
							<div class="formulario_grupo_input mb-1">
								<input type="text"
									class="form-control formulario_input_formulario"
									name="txtEmail" id="correo" required
									placeholder="Ingrese el correo electronico ej: juan@correo.com"
									value=""> <i class="formulario__validacion_estado"></i>
							</div>
							<!-- <p class="formulario_input_error mb-2">Minimo 7 digitos</p> -->
						</div>
						<br>
						<!-- Campo telefono -->
						<div class="col-12 col-lg-6" id="campo_telefono">
							<label for="telefono" class="formulario_label">Telefono</label>
							<div class="formulario_grupo_input mb-1">
								<input type="text"
									class="form-control formulario_input_formulario"
									name="txtTelefono" id="phone" required
									placeholder="Digite el n�mero de contacto" value=""> <i
									class="formulario__validacion_estado"></i>
							</div>
							<!-- <p class="formulario_input_error mb-2">Minimo 3 caracteres</p> -->
						</div>
						<br>
						<div class="col-12 col-lg-6">
							<button type="submit" id="buttonRegistrar"
								class="btn btn-primary" ><span>Registrar</span>
							</button>
						</div>
						<br> <label>�Ya tienes una cuenta?</label><a
							href="GameControlador?action=consultarAlquileres"
							class="colorlinea2"> Consultar mis alquileres</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
		crossorigin="anonymous"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<script src="_js/formulario.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
		integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
		integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
		crossorigin="anonymous"></script>

	<script src="js/jquery.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>