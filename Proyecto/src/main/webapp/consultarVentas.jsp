<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<meta charset="ISO-8859-1">
<title>WPOSS GAMES</title>
</head>
<body>
	<div class="wrapper d-flex align-items-stretch">
		<nav id="sidebar">
			<div class="custom-menu">
				<button type="button" id="sidebarCollapse" class="btn btn-primary">
					<i class="fa fa-bars"></i> <span class="sr-only">Games Menu</span>
				</button>
			</div>
			<div class="p-4 pt-5">
				<h1>
					<a href="GameControlador?action=indexPpal" class="logo">Men�</a>
				</h1>
				<ul class="list-unstyled components mb-5">
					<li class="active"><a href="GameControlador?action=addCliente">Agregar
							Cliente</a></li>
					<li><a href="GameControlador?action=addAlquilar">Alquilar
							Juego</a></li>
					<li><a href="GameControlador?action=modPrecio">Modificar
							Precio Alquiler</a></li>
					<li><a href="GameControlador?action=consultarVentas">Consultar
							Ventas</a></li>
					<li><a href="GameControlador?action=consultarAlquileres">Consultar
							Alquileres por Cliente</a></li>
				</ul>

			</div>
		</nav>

		<!-- Page Content  -->
		<div id="content">
			<div class="container">
				<div class="form-header">
					<br> <br>
					<h3>VENTAS DEL DIA</h3>
				</div>
				<table
					class="table table-striped table-bordered text-center table-hover table-condensed"
					style="margin-top: 20px;">
					<thead>
						<tr>
							<td>Fecha Venta</td>
							<td>ID Venta</td>
							<td>Cliente</td>
							<td>ID Alquiler</td>
							<td>Nombre Juego</td>
							<td>Consola</td>
							<td>Total Venta</td>
						</tr>
					</thead>
					<tbody>
						<!--Lista de las ventas del d�a  -->
						<c:forEach var="ventaXdia" items="${listaVentas}">
							<tr>
								<td>${ventaXdia.getFechaVenta()}</td>
								<td>${ventaXdia.getIdVenta()}</td>
								<td>${ventaXdia.getIdClienteVent()}</td>
								<td>${ventaXdia.getIdAlquilerVent()}</td>
								<td>${ventaXdia.getNombreJuego()}</td>
								<td>${ventaXdia.getNameConsola()}</td>
								<td>${ventaXdia.getTotVenta()}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>